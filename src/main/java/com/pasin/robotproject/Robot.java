/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.robotproject;

/**
 *
 * @author Pla
 */
public class Robot {
    private int x;
    private int y;
    private int bx;
    private int by;
    private int N;
    private char LastDirection = 'N';
    
    public Robot (int x, int y ,int bx ,int by ,int N){
       this.x = x;
       this.y = y;
       this.bx = bx;
       this.by = by;
       this.N = N;
    
    } 
    public boolean walk (char direction ){
        switch(direction){
            case 'N' :
                if(!inMap(x,y-1)){
                    printUnableMove();
                return false;
                }
                y=y-1;
                break;
            case 'S' :
                if(!inMap(x,y+1)){
                    printUnableMove();
                return false;
                }
                y=y+1;
                break;
            case 'E' :
                if(!inMap(x+1,y)){
                    printUnableMove();
                return false;
                }
                x=x+1;
                break;
            case 'W' :
                if(!inMap(x-1,y)){
                    printUnableMove();
                return false;
                }
                x=x-1;
                break;
        }
        LastDirection = direction;
        if(isBomb()){
            printBomb();
        }
        return true;
    }
    public boolean inMap (int x ,int y){
        if(x>=N||x<0 || y>=N||y<0){
            return false;
        }  
        return true;
}
    public boolean walk (char direction, int step){
        for(int i=0 ;i<step ;i++){
            if(!this.walk(direction)){
                return false;
            }
        }return true;
    }
    
    public boolean walk(){
        return this.walk(LastDirection);
    }
    
    public boolean walk(int step){
        return walk(LastDirection, step);
    }
    @Override
    public String toString(){
        return "Robot (" + this.x + " , " + this .y + ") Distance between bomb ("+Math.abs(this.x-bx) +","+Math.abs(this.y-by)+")" ;
    }
    public void printUnableMove() {
        System.out.println("i can't move!!!");
    }
    public void printBomb(){
        System.out.println("Bomb found");
    } 
    
    public boolean isBomb( ) {
        if(bx == x && by ==y){
            return true;
        }return false;
    }
}
