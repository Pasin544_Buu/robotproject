package com.pasin.robotproject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Pla
 */
public class TestRobot {
    public static void main(String[] args) {
        Robot robot = new Robot(0,0, 10,10, 100);
        System.out.println("Start "+robot);
        robot.walk('N');
        System.out.println(robot);
        robot.walk('W');
        System.out.println(robot);
        robot.walk('S',3);
        System.out.println(robot);
        robot.walk('E',2);
        System.out.println(robot);
        robot.walk('E',4);
        System.out.println(robot);
        robot.walk('S',7);
        System.out.println(robot);
        robot.walk('E',4);
        System.out.println(robot);
    }
}
